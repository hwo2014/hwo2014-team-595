(ns hwo2014bot.positions)

(defn car-position [p n]
  (some (fn [pos] (if (= n (get-in pos [:id :name])) pos)) p))

(defn piece-index [p]
  (get-in p [:piecePosition :pieceIndex]))

(defn piece-distance [p]
  (get-in p [:piecePosition :inPieceDistance]))
