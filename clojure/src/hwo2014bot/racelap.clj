(ns hwo2014bot.racelap
  (:require [hwo2014bot.positions :refer :all]
            [hwo2014bot.speed :refer :all]
            [hwo2014bot.track :refer :all]
            [hwo2014bot.state :refer [update-state]]))

(defn target-speed [position track state]
  0.65)

(defn perfect-throttle [cs ts state]
  ts)

(defn estimate-throttle [positions state]
  (let [po (car-position positions (:name state))
        cs (calculate-speed (:track state) (:position state) po)
        ts (target-speed po (:track state) state)]
    (perfect-throttle cs ts state)))

(defn update [positions state]
  (let [t (estimate-throttle positions state)
        pos (car-position positions (:name state))]
    [{:msgType "throttle" :data t}
     (update-state state {:position pos
                          :lap :race})]))


