(ns hwo2014bot.state)

(defn update-state [state map]
  (reduce (fn [s [k v]] (update-in s [k] (fn [_] v)))
          state
          map))

