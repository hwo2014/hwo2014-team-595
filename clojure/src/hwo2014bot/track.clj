(ns hwo2014bot.track)

(defn piece [s pi]
  (let [pieces (get-in s [:pieces])]
    (nth pieces (mod pi (count pieces)))))

(defn piece-seq
  ([track] (piece-seq track 0))
  ([track n] (cons
              (piece track n)
              (lazy-seq (piece-seq track (inc n))))))

(defn piece-length [p]
  (if (:length p) (:length p)
      (* (/ (Math/abs (:angle p)) 360) (* 2 Math/PI (+ (:radius p)
                                                       (if (> (:angle p) 0)
                                                         10 -10))))))

(defn seq-length [ps]
  (reduce + (map piece-length ps)))

(defn straight [track n]
  (take-while #(nil? (:angle %)) (piece-seq track n)))

(defn straight-length [track n]
  (seq-length (straight track n)))

(defn turn
  ([track n]
     (let [first (piece track n)]
       (if (nil? (:angle first))
         ()
         (cons first (turn track (inc n) (:angle first) (:radius first))))))
  ([track n angle radius]
     (take-while #(and (= radius (:radius %)) (>= (* angle (:angle %)) 0))
                 (piece-seq track n))))

(defn turn-length [track n]
  (seq-length (turn track n)))

(defn throttle-map [track]
  (let [pieces (:pieces track)]
    (map (fn [[idx piece]]
           (if (:length piece)
             (min 1.0 (* 0.003 (straight-length track idx)))
             (+ 0.5 (* 0.002 (:radius piece)))))
         (map vector
              (range (count pieces))
              pieces))))
