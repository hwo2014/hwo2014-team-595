(ns hwo2014bot.testlap
  (:require [hwo2014bot.positions :refer :all]
            [hwo2014bot.speed :refer :all]
            [hwo2014bot.track :refer :all]
            [hwo2014bot.state :refer [update-state]]))

(defn collect [positions state throttle kw]
  (let [p (car-position positions (:name state))
        v (or (calculate-speed (:track state) (:position state) p) 0.0)
        a (- v (or (:speed state) 0.0))
        t throttle
        d (Math/abs (- (:throttle state) (/ (:speed state) 10)))
        ad (if (= 0.0 d) 0 (/ a d))]
    [{:msgType "throttle" :data t}
     (update-state state {kw (conj (kw state) ad)
                          :throttle t})]))

(defn collect-accel [positions state]
  (collect positions state 0.8 :acceleration))

(defn collect-decel [positions state]
  (collect positions state 0.1 :deceleration))

(defn collect-turn [positions state]
  (let [p (car-position positions (:name state))
        pi (piece-index p)
        pc (piece (:track state) pi)
        r (:radius pc)
        v (or (calculate-speed (:track state) (:position state) p) 0.0)]
    [{:msgType "throttle" :data 1}
     (update-in state [:max-speeds r] (fn [pm] (max (or pm 0.0) v)))]))

(defn update [positions state]
  (let [p (car-position positions (:name state))
        pi (piece-index p)
        pc (piece (:track state) pi)
        [a d e] (:ad-test state)]
    (cond (and (>= pi a) (< pi d)) (collect-accel positions state)
          (and (>= pi d) (< pi e)) (collect-decel positions state)
          (not (nil? (:angle pc))) (collect-turn positions state)
          :default [{:msgType "throttle" :data 0.6} state])))
