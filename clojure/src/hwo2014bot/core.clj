(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.message :refer :all])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(def TRACE false)

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (when TRACE (println (format "Sending: %s" message)))
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (let [message (try (wait-for-message channel)
                     (catch Exception e
                       (println (str "ERROR: " (.getMessage e)))
                       (System/exit 1)))]
    (when TRACE (println (format "Received: %s" message)))
    (json->clj message)))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn log-exception [e state msg]
  (println (format "Exception: %s, state: %s, msg: %s" e state msg))
  (.printStackTrace e))

(defn game-loop [channel state]
  (let [msg (read-message channel)
        [r s] (try (handle-msg msg state)
                   (catch Exception e
                     (log-exception e state msg)
                     (System/exit 1)))]
    (log-msg msg)
    (when-not (nil? r) (send-message channel r))
    (recur channel s)))

(defn single-race-join-msg [botname botkey]
  {:msgType "join" :data {:name botname :key botkey}})

(defn duel-race-join-msg [botname botkey]
  {:msgType "joinRace" :data {:botId {:name botname :key botkey}
                              :carCount 2}})

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel (single-race-join-msg botname botkey))
    (game-loop channel {})))
