(ns hwo2014bot.distance
  (:require [hwo2014bot.track :refer :all]))

(defn distance
  ([track start end]
     (distance track start end 0))
  ([track start end sum]
     (cond
      (>= start end)
      sum
      :default
      (recur track
             (inc start)
             end
             (+ sum (piece-length (piece track start)))))))



