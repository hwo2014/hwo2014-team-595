(ns hwo2014bot.speed
  (:require [hwo2014bot.positions :refer :all]
            [hwo2014bot.distance :refer :all]))

(defn calculate-speed [track prev cur]
  (if (or (nil? track) (nil? prev) (nil? cur)) nil
      (let [prev-pi (piece-index prev)
            prev-pd (piece-distance prev)
            cur-pi (piece-index cur)
            cur-pd (piece-distance cur)
            dist (distance track prev-pi cur-pi)]
        (- (+ cur-pd dist) prev-pd))))


