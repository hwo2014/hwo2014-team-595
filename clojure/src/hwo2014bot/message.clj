(ns hwo2014bot.message
  (:require [hwo2014bot.testlap :as testlap]
            [hwo2014bot.racelap :as racelap]
            [hwo2014bot.track :as track]
            [hwo2014bot.state :refer [update-state]]
            [hwo2014bot.positions :refer [car-position]]
            [hwo2014bot.speed :refer [calculate-speed]]))

(defmulti handle-msg
  (fn [m _] (:msgType m)))

(defmethod handle-msg "carPositions" [msg state]
  (let [poss (:data msg)
        pos (car-position poss (:name state))
        v (or (calculate-speed (:track state) (:position state) pos) 0.0)
        [m s] (if (= 0 (get-in pos [:piecePosition :lap]))
                (testlap/update poss state)
                (racelap/update poss state))]
    [m (update-state s {:position pos :speed v})]))

(defmethod handle-msg "yourCar" [msg state]
  [nil (update-in state [:name] (fn [_] (get-in msg [:data :name])))])

(defmethod handle-msg "gameInit" [msg state]
  (let [track (get-in msg [:data :race :track])]
    [nil (update-state state {:track track
                              :ad-test [0 2 4]
                              :speed 0.0
                              :throttle 0.0
                              :acceleration []
                              :deceleration []
                              :max-speeds {}})]))

(defmethod handle-msg "gameEnd" [msg state]
  (let [acc-data (drop 2 (:acceleration state))
        dec-data (drop 2 (:deceleration state))]
    (when (not (empty? acc-data)) (println (/ (apply + acc-data) (count acc-data))))
    (when (not (empty? dec-data)) (println (/ (apply + dec-data) (count dec-data))))
    (println (:max-speeds state))))

(defmethod handle-msg :default [msg state]
  [{:msgType "ping" :data "ping"} state])

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))


